package com.fangbaba.basic.face.service;

import java.util.List;

import com.fangbaba.basic.face.bean.DistrictModel;

public interface DistrictService {
	List<DistrictModel> queryAllDistricts();
}
