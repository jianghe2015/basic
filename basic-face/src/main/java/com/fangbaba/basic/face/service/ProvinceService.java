package com.fangbaba.basic.face.service;

import java.util.List;

import com.fangbaba.basic.face.bean.ProvinceModel;

public interface ProvinceService {
	List<ProvinceModel> queryAllProvinces();
}
