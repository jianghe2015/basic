package com.fangbaba.basic.util;

/**
 * 搜索常量定义类.
 * @author chuaiqing.
 *
 */
public final class SearchConst {
    /** 酒店搜索默认页码 */
    public final static Integer SEARCH_PAGE_DEFAULT = 1;
    
    /** 酒店搜索默认条数 */
    public final static Integer SEARCH_LIMIT_DEFAULT = 10;
    
    /** 酒店搜索默认搜索半径（单位: 米） */
    public final static Integer SEARCH_RANGE_DEFAULT = 5000;
    
    /** 酒店搜索最大搜索半径（单位: 米） */
    public final static Integer SEARCH_RANGE_MAX = 3000000;

}
